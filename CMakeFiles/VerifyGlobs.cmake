# CMAKE generated file: DO NOT EDIT!
# Generated by CMake Version 3.24
cmake_policy(SET CMP0009 NEW)

# sources at CMakeLists.txt:20 (file)
file(GLOB_RECURSE NEW_GLOB FOLLOW_SYMLINKS LIST_DIRECTORIES false "/Users/anastasiapronina/assignment-4-memory-allocator/src/*.c")
set(OLD_GLOB
  "/Users/anastasiapronina/assignment-4-memory-allocator/src/main.c"
  "/Users/anastasiapronina/assignment-4-memory-allocator/src/mem.c"
  "/Users/anastasiapronina/assignment-4-memory-allocator/src/mem_debug.c"
  "/Users/anastasiapronina/assignment-4-memory-allocator/src/util.c"
  )
if(NOT "${NEW_GLOB}" STREQUAL "${OLD_GLOB}")
  message("-- GLOB mismatch!")
  file(TOUCH_NOCREATE "/Users/anastasiapronina/assignment-4-memory-allocator/CMakeFiles/cmake.verify_globs")
endif()

# sources at CMakeLists.txt:20 (file)
file(GLOB_RECURSE NEW_GLOB FOLLOW_SYMLINKS LIST_DIRECTORIES false "/Users/anastasiapronina/assignment-4-memory-allocator/src/*.h")
set(OLD_GLOB
  "/Users/anastasiapronina/assignment-4-memory-allocator/src/mem.h"
  "/Users/anastasiapronina/assignment-4-memory-allocator/src/mem_internals.h"
  "/Users/anastasiapronina/assignment-4-memory-allocator/src/util.h"
  )
if(NOT "${NEW_GLOB}" STREQUAL "${OLD_GLOB}")
  message("-- GLOB mismatch!")
  file(TOUCH_NOCREATE "/Users/anastasiapronina/assignment-4-memory-allocator/CMakeFiles/cmake.verify_globs")
endif()
