#include <assert.h>
#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"

#define HEAP_SIZE 1024

void test_success_memory_allocation(FILE *f){
    void *heap = heap_init(HEAP_SIZE);
    assert(heap != NULL);
    debug_heap(f, heap);
    heap_term();
}

void test_one_block_released(FILE *f){
    void *heap = heap_init(HEAP_SIZE);
    assert(heap != NULL);
    debug_heap(f, heap);
    void *block1 = _malloc(128);
    assert(block1 != NULL);
    void *block2 = _malloc(128);
    assert(block2 != NULL);
    debug_heap(f, heap);
    _free(block1);
    debug_heap(f, heap);
    _free(block2);
    heap_term();
}

void test_two_blocks_released(FILE *f){
    void *heap = heap_init(HEAP_SIZE);
    assert(heap != NULL);
    debug_heap(f, heap);
    void *block1 = _malloc(128);
    void *block2 = _malloc(128);
    void *block3 = _malloc(128);
    void *block4 = _malloc(128);
    debug_heap(f, heap);
    _free(block3);
    _free(block2);
    debug_heap(f, heap);
    _free(block1);
    _free(block4);
    heap_term();
}

void test_heap_cont_growth(FILE *f){
    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(f, heap);
    void *block1 = _malloc(128);
    void *block2 = _malloc(128);
    void *block3 = _malloc(128);
    debug_heap(f, heap);
    void *block4 = _malloc(REGION_MIN_SIZE * 8);
    debug_heap(f, heap);
    _free(block1);
    _free(block2);
    _free(block3);
    _free(block4);
    heap_term();
}

void test_heap_not_cont_growth(FILE *f){
    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(f, heap);
    struct block_header *header = (struct block_header *)heap;
    size_t offset = size_from_capacity(header->capacity).bytes;
    void *block1 = _malloc(128);
    void *block2 = _malloc(128);
    void *block3 = _malloc(128);
    debug_heap(f, heap);
    printf("%lu\n", offset);
    printf("%p\n", heap + offset);
    void *mem = mmap(heap + offset, 24, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0);
    printf("%p", mem);
    void *block4 = _malloc(REGION_MIN_SIZE * 8);
    debug_heap(f, heap);
    _free(block1);
    _free(block2);
    _free(block3);
    _free(block4);
    munmap(heap + offset, 24);
    heap_term();
}


int main(){
    test_success_memory_allocation(stdout);
    test_one_block_released(stdout);
    test_two_blocks_released(stdout);
    test_heap_cont_growth(stdout);
    test_heap_not_cont_growth(stdout);
}