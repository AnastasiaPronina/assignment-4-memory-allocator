#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static block_size sum_block_size(block_size b1, block_size b2){
  return (block_size){
    .bytes = b1.bytes + b2.bytes
  };
}

static block_size deduct_block_size(block_size minuend, block_size subtrahend){
  return (block_size){
    .bytes = minuend.bytes - subtrahend.bytes
  };
}

/* Является ли блок достаточно большим, чтобы вместить указанное количество байт */
static bool block_is_big_enough( size_t query, struct block_header* block ) { 
  return block->capacity.bytes >= query; 
}

static size_t pages_count ( size_t mem ) { 
  return mem / getpagesize() + ((mem % getpagesize()) > 0); 
}

static size_t round_pages ( size_t mem ) { 
  return getpagesize() * pages_count( mem ); 
}

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { 
  return size_max( round_pages( query ), REGION_MIN_SIZE ); 
}

extern inline bool region_is_invalid( const struct region* r );


/* Возращаем указатель на память, выделенную функцией mmap. В функцию mmap передается
адрес, по которому хотим инициализировать память, затем число байт, потом флаги доступа к этой области
памяти, далее дополнительные флаги, влияющие на свойства отображения, потом файловый дескриптор 
и offset - смещение данных относительно начала файла */
static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
/* передаем адрес, по которому хотим аллоцировать регион, и его желаемый размер */
static struct region alloc_region  ( void const * addr, size_t query ) {
  block_size real_block_size = size_from_capacity((block_capacity){.bytes = query});
  size_t real_region_size = region_actual_size(real_block_size.bytes);
  real_block_size.bytes = size_max(real_block_size.bytes, real_region_size);
  // флаг map_fixed гарантирует, что память выделится по указанному адресу
  bool extends = true;
  void* memory = map_pages(addr, real_region_size, MAP_FIXED);
  if (memory == MAP_FAILED){
    memory = map_pages(addr, real_region_size, 0);
    extends = false;
  }
  if (memory == MAP_FAILED){
    return REGION_INVALID;
  }
  block_init(memory, real_block_size, NULL);
  return (struct region) {
    .addr = memory,
    .size = real_region_size,
    .extends = extends
  };
}

static void* block_after( struct block_header const* block )         ;

/* Инициализировать кучу указанного размера (один регион) */
void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) 
    return NULL;
  return region.addr;
}

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous ( struct block_header const* fst,
                                struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

/*  освободить всю память, выделенную под кучу */
void heap_term() {
  
  struct block_header *first_region_header = (struct block_header*) HEAP_START;
  while (first_region_header){
    size_t size_to_free = 0;
    struct block_header *this = first_region_header;
    struct block_header *next = this->next;
    while(blocks_continuous(this, next)){
      size_to_free += size_from_capacity(first_region_header->capacity).bytes;
      this = this->next;
      next = next->next;
    }
    size_to_free += size_from_capacity(this->capacity).bytes;
    munmap(first_region_header, size_to_free);
    first_region_header = next;
  }
  
}

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  query = size_max(query, BLOCK_MIN_CAPACITY);
  if (block && block_splittable(block, query)){
    block_size fst_size = size_from_capacity((block_capacity) {.bytes = query});
    block_size snd_size = deduct_block_size(size_from_capacity(block->capacity), fst_size);
    
    void *next = block->next;
    void *fst_addr = (void *)block;
    
    void *snd_addr = (void *)block + fst_size.bytes;
    
    block_init(fst_addr, fst_size, snd_addr);
    block_init(snd_addr, snd_size, next);
    
    return true;
  } else {
    return false;
  }
}


/*  --- Слияние соседних свободных блоков --- */

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if (block && block->next && mergeable(block, block->next)){
    block_size new_block_size = sum_block_size(size_from_capacity(block->capacity), size_from_capacity((block->next)->capacity));
    block_init(block, new_block_size, (block->next)->next);
    return true;
  } else {
    return false;
  }
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {
    BSR_FOUND_GOOD_BLOCK, 
    BSR_REACHED_END_NOT_FOUND, 
    BSR_CORRUPTED
  } type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  struct block_search_result result = {.type = BSR_CORRUPTED, .block = block};
  struct block_header *last = block;
  if (!block){
    return result;
  }
  while (block){ 
    if (try_merge_with_next(block)){
      continue;
    }
    if (block->is_free && block_is_big_enough(sz, block)){
      result.type = BSR_FOUND_GOOD_BLOCK;
      result.block = block;
      return result;
    }
    last = block;
    block = block->next;
  }
  result.type = BSR_REACHED_END_NOT_FOUND;
  result.block = last;
  return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result res = find_good_or_last(block, query);
  if (res.type == BSR_FOUND_GOOD_BLOCK){
    split_if_too_big(res.block, query);
    (res.block)->is_free = false;
  }
  return res;
}


static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  void *addr = block_after(last);
  //void *addr = (void *)last + size_from_capacity(last->capacity).bytes;
  struct region new_region = alloc_region(addr, query);
  if (new_region.addr == NULL){
    return NULL;
  }
  last->next = new_region.addr;
  if (last->is_free && new_region.extends){
    try_merge_with_next(last);
  }
  if (last->next){
    return last->next;
  } else {
    return last;
  }
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */

static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  query = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result result = try_memalloc_existing(query, heap_start); 
  switch (result.type){
    case BSR_FOUND_GOOD_BLOCK: {
      return result.block;
    };
    case BSR_REACHED_END_NOT_FOUND: {
      return try_memalloc_existing(query, grow_heap(result.block, query)).block;
    };
    case BSR_CORRUPTED: {
      return NULL;
    };
    default: {
      return NULL;
    }
  }
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) 
    return addr->contents;
  else 
    return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  try_merge_with_next(header);
}
